RailsAdmin.config do |config|

  config.main_app_name = ["UnBRendimento", ""]

  require Rails.root.join('lib', 'rails_admin', 'rails_admin_pdf.rb')
  RailsAdmin::Config::Actions.register(RailsAdmin::Config::Actions::Pdf)

  ### Popular gems integration
  config.parent_controller = 'ApplicationController'

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == CancanCan ==
  RailsAdmin.config do |config|
	config.authorize_with :cancancan
  end

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.model Aviso do
	navigation_icon 'fa fa-exclamation-circle'
	create do
	  field  :aviso
   
	  field :user_id, :hidden do
		default_value do
		  bindings[:view]._current_user.id
		end
	  end
	end
   
	edit do
		field  :aviso
	 
		field :user_id, :hidden do
		  default_value do
			bindings[:view]._current_user.id
		  end
		end
	  end

	show do
		field :aviso
	end

	list do
		field :id
		field :aviso
	end
  end

	config.model User do
		navigation_icon 'fa fa-address-card'
		create do
			field  :nome
			field  :tipo_usuario
			field  :status
			field  :email
			field  :password
			field  :password_confirmation

		end
	
		edit do
			field  :nome
			field  :tipo_usuario
			field  :status
			field  :email
			field  :password
			field  :password_confirmation
		end

		show do 
			field  :nome
			field  :tipo_usuario
			field  :status
			field  :email
		end

		list do
			field :id
			field :nome
			field :tipo_usuario
			field :email
		end
	end

	config.model Aluno do
		navigation_icon 'fa fa-id-badge'
		create do
		  field  :matricula
		  field  :user
		  field  :rendimento
	
		end
	   
		edit do
			field  :matricula
			field  :user
		    field  :rendimento
		end
	
		show do 
			field  :matricula
		    field  :rendimento
		end

		list do
			field :id
			field :matricula
			field :user
			field :rendimento
		end
	end

	config.model Rendimento do
		navigation_icon 'fa fa-book'
		create do
		  field  :prova_1
		  field  :prova_2
		  field  :trabalho_1
		  field  :trabalho_2
		  field  :trabalho_3
		  field  :aviso
		  field  :status
		  field  :aluno

		  field :user_id, :hidden do
				default_value do
					bindings[:view]._current_user.id
				end
		  end
	
		end
	   
		edit do
		  field  :prova_1
		  field  :prova_2
		  field  :trabalho_1
		  field  :trabalho_2
		  field  :trabalho_3
		  field  :aviso
		  field  :status
		#   field  :aluno
		  field :user_id, :hidden do
			default_value do
				bindings[:view]._current_user.id
			end
	      end
		end
	
		show do 
		  field  :prova_1
		  field  :prova_2
		  field  :trabalho_1
		  field  :trabalho_2
		  field  :trabalho_3
		  field  :nota_final
		  field  :mencao_final
		  field  :aviso
		end

		list do
			field :id
			field :nota_final
			field :mencao_final
		end
	end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    # export
    bulk_delete
    show
    edit
    delete
	show_in_app
	pdf do
		only Aluno 
	end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
