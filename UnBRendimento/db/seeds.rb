# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Criando nossos Users --- OBS: Depois que adicionarmos o devise precisamos incluir o email e senha dos users

User.create nome: 'Lucas Ursulino Boaventura', status: :ativo, tipo_usuario: :professor, email: 'admin@teste.com', password: "admin123"
User.create nome: 'Daniel de Melo', tipo_usuario: :monitor, status: :ativo, email: 'monitor1@teste.com', password: "monitor123"
User.create nome: 'Natália Schulz', tipo_usuario: :monitor, status: :ativo, email: 'monitor2@teste.com', password: "monitor123"
User.create nome: 'Igor Gomes', status: :ativo, tipo_usuario: :aluno, email: 'aluno1@teste.com', password: "aluno123"	