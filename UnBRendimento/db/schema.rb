# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_02_191728) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alunos", force: :cascade do |t|
    t.integer "matricula"
    t.bigint "user_id", null: false
    t.bigint "rendimento_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rendimento_id"], name: "index_alunos_on_rendimento_id"
    t.index ["user_id"], name: "index_alunos_on_user_id"
  end

  create_table "avisos", force: :cascade do |t|
    t.text "aviso"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_avisos_on_user_id"
  end

  create_table "rendimentos", force: :cascade do |t|
    t.float "prova_1"
    t.float "prova_2"
    t.float "trabalho_1"
    t.float "trabalho_2"
    t.float "trabalho_3"
    t.float "nota_final"
    t.string "mencao_final"
    t.text "aviso"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status"
    t.bigint "aluno_id"
    t.integer "user_id"
    t.index ["aluno_id"], name: "index_rendimentos_on_aluno_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nome"
    t.integer "tipo_usuario"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.bigint "aviso_id"
    t.index ["aviso_id"], name: "index_users_on_aviso_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "alunos", "rendimentos"
  add_foreign_key "alunos", "users"
  add_foreign_key "avisos", "users"
  add_foreign_key "rendimentos", "alunos"
  add_foreign_key "users", "avisos"
end
