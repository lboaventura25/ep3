class AddAvisosToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :aviso, null: true, foreign_key: true
  end
end
