class AddStatusToRendimento < ActiveRecord::Migration[6.0]
  def change
    add_column :rendimentos, :status, :integer
  end
end
