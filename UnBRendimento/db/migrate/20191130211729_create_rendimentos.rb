class CreateRendimentos < ActiveRecord::Migration[6.0]
  def change
    create_table :rendimentos do |t|
      t.float :prova_1
      t.float :prova_2
      t.float :trabalho_1
      t.float :trabalho_2
      t.float :trabalho_3
      t.float :nota_final
      t.string :mencao_final
      t.text :aviso

      t.timestamps
    end
  end
end
