class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :nome
      t.integer :tipo_usuario
      t.integer :status

      t.timestamps
    end
  end
end
