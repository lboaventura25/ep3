class AddAlunoToRendimento < ActiveRecord::Migration[6.0]
  def change
    add_reference :rendimentos, :aluno, null: true, foreign_key: true
  end
end
