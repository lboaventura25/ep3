class CreateAlunos < ActiveRecord::Migration[6.0]
  def change
    create_table :alunos do |t|
      t.integer :matricula
      t.references :user, null: false, foreign_key: true
      t.references :rendimento, null: true, foreign_key: true

      t.timestamps
    end
  end
end
