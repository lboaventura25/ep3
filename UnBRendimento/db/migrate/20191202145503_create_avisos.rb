class CreateAvisos < ActiveRecord::Migration[6.0]
  def change
    create_table :avisos do |t|
      t.text :aviso
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
