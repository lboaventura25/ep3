class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
	devise :database_authenticatable, :registerable,
			:recoverable, :rememberable, :validatable
	enum tipo_usuario: [:monitor, :professor, :aluno]
	enum status: [:ativo, :inativo]
	has_many :alunos 
	has_many :rendimentos
	has_many :avisos

	before_save do
		if self.tipo_usuario
			
		else
			self.tipo_usuario = "aluno"
		end
	end
end
