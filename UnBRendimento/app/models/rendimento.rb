class Rendimento < ApplicationRecord
	enum status: [:ativo, :inativo]
	has_one :aluno

	before_save do
		mencao = "SR"
		nota = ((((self.prova_1 + self.trabalho_1) / 2.0) + 
					  ((self.prova_2 + self.trabalho_2) / 2.0) +
						self.trabalho_3) / 3.0)

		if nota < 3.0
			mencao = "II"
		elsif nota < 5.0
			mencao = "MI"
		elsif nota < 7.0
			mencao = "MM"
		elsif nota < 9.0
			mencao = "MS"
		elsif nota >= 9.0 
			mencao = "SS"
		else
			mencao = "SR"
		end
		
		self.mencao_final = mencao
		self.nota_final = nota.round(2)

	end
end
