class Ability
	include CanCan::Ability
   
	def initialize(user)
	  if user
		if user.tipo_usuario == 'professor'
			can :manage, :all
		elsif user.tipo_usuario == 'aluno'
		  	# can :manage, :all
			  can :access, :rails_admin
			  can :read, :dashboard
			  can :read, [Aluno, Rendimento, Aviso]
			#   can :read 
			#   can :manage, Aluno, status: :ativo, user_id: user.id
			#   can :manage, Rendimento, status: :ativo, user_id: user.id
		else
			can :access, :rails_admin
			can :read, :dashboard
			can :read, Aluno, user_id: user.id
			can :manage, Rendimento
		end
	  end
	end
  end
