module RailsAdminPdf
end

require 'rails_admin/config/actions'
require 'prawn'

module RailsAdmin
	module Config
		module Actions
			class Pdf < Base
				RailsAdmin::Config::Actions.register(self)

				register_instance_option :member do
					true
				end

				register_instance_option :pjax? do
					false
				end

				register_instance_option :controller do
					Proc.new do

						# Configura Pdf
						PDF_OPTIONS = {
							:page_size   => "A4",
							:page_layout => :portrait,
							:margin      => [40, 75]
						}

						ramdom_file_name = (0...8).map { (65 + rand(26)).chr }.join

						Prawn::Document.new(PDF_OPTIONS) do |pdf|
							pdf.fill_color "666666"
							pdf.text "Relatório do Rendimento", :size => 32, :style => :bold, :align => :center
							pdf.move_down 80
							
								pdf.text "Aluno da Turma de OO", :size => 14, :align => :justify, :inline_format => true, :style => :bold
								pdf.move_down 8

									if @object.matricula
										pdf.text "Matrícula: #{@object.matricula}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.prova_1
										pdf.text "Prova 1: #{@object.rendimento.prova_1}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.prova_2
										pdf.text "Prova 2: #{@object.rendimento.prova_2}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.trabalho_1
										pdf.text "Trabalho 1: #{@object.rendimento.trabalho_1}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.trabalho_2
										pdf.text "Trabalho 2: #{@object.rendimento.trabalho_2}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.trabalho_3
										pdf.text "Trabalho 3: #{@object.rendimento.trabalho_3}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.nota_final
										pdf.text "Nota Final: #{@object.rendimento.nota_final}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end

									if @object.rendimento.mencao_final
										pdf.text "Menção Final: #{@object.rendimento.mencao_final}", :size => 12, :align => :justify, :inline_format => true
										pdf.move_down 8
									end
							

							pdf.font "Helvetica"
							pdf.text "Link Para o Manual do Prawn clicável", :size => 10, :inline_format => true, :valign => :bottom, :align => :left
							pdf.number_pages "Gerado: #{(Time.now).strftime("%d/%m/%y as %H:%M")} - Página ", :start_count_at => 0, :page_filter => :all, :at => [pdf.bounds.right - 140, 7], :align => :right, :size => 8
							pdf.render_file("public/#{ramdom_file_name}.pdf")
						end
						
						File.open("public/#{ramdom_file_name}.pdf", 'r') do |f|
							send_data f.read.force_encoding('BINARY'), :filename => 'pdf', :type => "application/pdf", :disposition => "attachment"
						end
							File.delete("public/#{ramdom_file_name}.pdf")
					end
				end

				register_instance_option :link_icon do
					'icon-folder-open'
				end
			end
		end
	end
end