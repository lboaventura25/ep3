# EP3 - OO 2019.2 - UnB - FGA
> Alunos: Daniel Primo de Melo e Lucas Ursulino Boaventura
>
> Matrículas: 18/0063162 e 18/0114093
# UnBRendimento
O projeto tem como objetivo implementar um programa que facilita a 
visualização dos alunos de seus respectivos rendimentos na matéria
de Orientação a Objetos, expecificamente com a metodologia do professor Renato Coral.
O projeto também visa a melhoria do processo de correção de trabalhos,
tanto para professores como para monitores.
## Versões usadas
    ruby 2.6.5p114
    Rails 6.0.1
    rbenv 1.1.1-39
## Diagrama de classe
![img](./UnBRendimento/diagrama/diagrama_de_classes_UnBRendimento.jpg)
## Como usar
1. Execute os seguintes comandos no seu terminal
```shell
   $ git clone https://gitlab.com/lboaventura25/ep3
   $ cd ep3/UnBRendimento
   $ bundle install
   $ rake db:create db:migrate
```
2. Caso queira iniciar a aplicação com alguns usuários padrão
```shell
   $ rake db:seed
```
3. Os usuários já incluidos e suas senhas, respectivamente, são:
```shell
    admin@teste.com - admin123  (Usuário Professor)
    monitor1@teste.com - monitor123  (Usuário Monitor 1)
    monitor2@teste.com - monitor123  (Usuário Monitor 2)
    aluno1@teste.com - aluno123  (Usuário Aluno 1)
```
4. Foi feito um deploy da aplicação e você também pode acessar a aplicação pelo [site do UnBRendimento](http://unb-rendimento.herokuapp.com)
    
    
## A Aplicação UnBrendimento
A aplicação consiste na adminstração de notas e rendimento dos alunos, mundando a forma e as permissões que cada perfil de usuário tem acesso.
### Professor
O Usuário **Professor** é o principal administrador da aplicação, podendo fazer:
- Alunos:
    O Professor pode criar um novo aluno, editar um existente ou excluir.
- Avisos:
    O Professor pode criar avisos, que serão entregues para os alunos.
- Rendimentos:
    O Professor pode lançar as notas do aluno. Essa função também calcula a média final e menção.
- Usuários:
    O Professor pode criar um novo usuário, editar um existente ou excluir.
### Monitor
O Usuário **Monitor** pode fazer:
- Alunos:
    O Monitor pode visualizar todos os alunos cadastrados.
- Rendimentos:
    O Monitor pode lançar as notas do aluno. Essa função também calcula a média final e a nota final.
### Aluno
O Usuário **Aluno** pode fazer:
- Alunos:
    O Aluno pode visualizar todos os alunos cadastrados.
- Rendimentos:
    O Aluno pode visualizar os rendimentos lançados por Professor ou Monitor.
- Avisos:
    O Aluno pode visualizar todos os avisos lançados pelo Professor.



# ep3

Projeto 3 da disciplina Orientação a Objetos

# Requisitos Mínimos do EP3
  
1. Projeto de Aplicativo Web em Ruby on Rails

1. Incluir em sua modelagem pelo menos duas classes de dados (model) além dos usuários.

1. A aplicação deverá gerenciar o cadastro de usuários com acesso à funções exclusivas, além das funcionalidades públicas da aplicação.

1. Interface com layout elaborado para além do padrão do Rails (Usando CSS, Bootstrap, Matrerial design, etc.)

1. Descrição da solução completa no arquivo Readme.md do repositório contendo: descrição da solução com os objetivos e funcionalidades, o diagrama de classes tanto das Models quanto das Controllers, o diagrama de casos de uso da aplicação.

 
# Requisitos para maior pontuação no trabalho

1. Criar relação entre as models

1. Gerência de níveis de permissão por tipo de usuário (usuário, regular, usuário admin, etc)

1. Uso de outras gems, além das que já são padrão no projeto inicial do Rails.

1. Relevância do tema, criatividade e refinamento na implementação.


# Entrega do EP3 consistirá de:

1. Link para o repositório GIT com o código fonte da aplicação.